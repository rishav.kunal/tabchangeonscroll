package com.example.testapp;

import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView list;
    private TabLayout tabLayout;
    private ArrayList<Product> items;
    private LinearLayoutManager layoutManager;
    private int firstVisibleItem=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        list = findViewById(R.id.recyclerview);
        tabLayout = findViewById(R.id.tab_layout);
        items = new ArrayList();




        tabLayout.addTab(tabLayout.newTab().setText("Thali"));
        tabLayout.addTab(tabLayout.newTab().setText("Pizza"));
        tabLayout.addTab(tabLayout.newTab().setText("Meal"));


        //tabLayout.addTab(secondTab);
        //tabLayout.addTab(thirdTab);
        addProducts();
        layoutManager = new LinearLayoutManager(this);
        list.setAdapter(new ProductListAdapter(items,this));
        list.setLayoutManager(layoutManager);
        TabLayout.OnTabSelectedListener tabSelectedListener = new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()){
                    case 0 : layoutManager.scrollToPositionWithOffset(0,0);
                        break;
                    case 1 : layoutManager.scrollToPositionWithOffset(6,0);
                        break;
                    case 2 : layoutManager.scrollToPositionWithOffset(17,0);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        };

        tabLayout.addOnTabSelectedListener(tabSelectedListener);

        //tabLayout.setOnTabSelectedListener(tabSelectedListener);

        list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                TabLayout.Tab tab;
                if (firstVisibleItem >= 0 && firstVisibleItem < 4){
                    tab = tabLayout.getTabAt(0);
                    tab.select();
                } else if (firstVisibleItem >= 4 && firstVisibleItem < 12){
                    tab = tabLayout.getTabAt(1);
                    tab.select();
                } else if (firstVisibleItem >= 12 && firstVisibleItem < 22){
                    tab = tabLayout.getTabAt(2);
                    tab.select();
                }
//                switch (firstVisibleItem){
//                    case 0 : tab = tabLayout.getTabAt(0);
//                        tab.select();
//                        break;
//                    case 1 : tab = tabLayout.getTabAt(0);
//                        tab.select();
//
//                        break;
//                    case 2 : tab = tabLayout.getTabAt(0);
//                    //tab.setText("First");
//                        tab.select();
//                        break;
//                    case 4 : tab = tabLayout.getTabAt(1);
//                        tab.select();
//                        break;
//                    case 7 : tab = tabLayout.getTabAt(1);
//                        tab.select();
//                        break;
//                    case 10 : tab = tabLayout.getTabAt(1);
//                        tab.select();
//                        break;
//                    case 15 : tab = tabLayout.getTabAt(2);
//                        tab.select();
//                        break;
//                    case 18 : tab = tabLayout.getTabAt(2);
//                        tab.select();
//                        break;
//                    case 20 : tab = tabLayout.getTabAt(2);
//                        tab.select();
//                        break;
//                    case 22 : tab = tabLayout.getTabAt(2);
//                        tab.select();
//                        break;
//                }
            }
        });


    }

    private void addProducts(){
        //header
        Product product = new Product("Thali","Thali");
        product.type =1;
        items.add(product);
        //header end
        product = new Product("Afternoon Thali","Thali");
        items.add(product);
        product = new Product("Afternoon Thali","Thali");
        items.add(product);
        product = new Product("Veg Thali","Thali");
        items.add(product);
        items.add(product);
        items.add(product);
        items.add(product);
        //header
        product = new Product("Pizza","Pizza");
        product.type =1;
        items.add(product);
        //header end
        product = new Product("Medium Margharita","Pizza");
        items.add(product);
        items.add(product);
        items.add(product);
        product = new Product("Reg Margharita","Pizza");
        items.add(product);
        items.add(product);
        items.add(product);
        items.add(product);
        items.add(product);
        items.add(product);
        items.add(product);
        //header
        product = new Product("Meal","Meal");
        product.type =1;
        items.add(product);
        //header end
        product = new Product("South Indian Meal","Meal");
        items.add(product);
        items.add(product);
        items.add(product);
        items.add(product);
        items.add(product);
        items.add(product);
        //header
        product = new Product("Frequently","Frequently");
        product.type =1;
        items.add(product);
        //header end
        product = new Product("Egg White Burji","Frequently");
        items.add(product);
        items.add(product);
        items.add(product);
        items.add(product);
        items.add(product);
        items.add(product);
    }
}
