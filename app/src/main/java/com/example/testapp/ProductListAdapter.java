package com.example.testapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ProductVH> {
    ArrayList<Product> productList;
    Context context;

    public ProductListAdapter(ArrayList<Product> productList,Context context) {
        this.productList = productList;
        this.context = context;
    }

    @NonNull
    @Override
    public ProductVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView;
        if (i==0){
            itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.header, viewGroup, false);
        }
        else {
            itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_list_item, viewGroup, false);
        }
        return new ProductVH(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull ProductVH productVH, int i) {

        productVH.name.setText(productList.get(i).getName());
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (productList.get(position).type == 1){
            return  0;
        }
        else return 1;
    }

    class ProductVH extends RecyclerView.ViewHolder{
        TextView name;
        public ProductVH(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.tv_name);
        }
    }

    class HeaderVH extends RecyclerView.ViewHolder{
        TextView header;
        public HeaderVH(@NonNull View itemView) {
            super(itemView);
            header = itemView.findViewById(R.id.tv_name);
        }
    }
}
